import os

def getConfig():
    env = os.environ.get('FLASK_ENV')
    if env == 'development':
        return DevelopmentConfig()
    else:
        return ProductionConfig()

class ProductionConfig():
    REDIS_HOST = '******'
    REDIS_PORT = 6379

class DevelopmentConfig():
    REDIS_HOST = 'localhost'
    REDIS_PORT = 6379