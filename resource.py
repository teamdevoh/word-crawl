from flask import Flask
from flask import request
import requests
import redis
import math
import json
import resource

def getList():
    titles = []
    page = 100

    term = 'tuberculosis'
    days = '365' # 365 days

    searchUrl = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?retmode=json&db=pubmed&term=' + term + '&reldate=' + days
    summaryUrl = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?retmode=json&db=pubmed'


    res = requests.get(searchUrl + '&rettype=count')
    if(res.status_code == 200):
        data = json.loads(res.text)['esearchresult']
        totalCount = int(data['count'])
        
        pageSize = math.ceil(round(totalCount / page, 1))

        currentPage = 0

        res = requests.get(searchUrl + '&retmax=10000')
        data = json.loads(res.text)['esearchresult']
        pmids = data['idlist']


        start = 1
        end = start + page
        
        while(currentPage <= pageSize):
            currentPage = currentPage + 1
            
            joinedIds = ",".join(pmids[start : end])
            resSummary = requests.get(summaryUrl + '&id='+ joinedIds)

            start = currentPage * page + 1
            end = start + page

            if(resSummary.status_code == 200):
                text = json.loads(resSummary.text)
                if 'result' in text:
                    data = text['result']
                    uids = data['uids']

                    for id in uids:
                        titles.append(data[str(id)]['title'])

    return titles