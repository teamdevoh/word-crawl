from flask import Flask, render_template, request, jsonify
import redis
import json
import resource
import util
import config
from apyori import apriori
import pandas as pd
import networkx as nx
from collections import Counter
from networkx.readwrite import json_graph


app = Flask(__name__)

@app.route("/wordcloud", methods=["GET"])
def getResource():
    env = config.getConfig()
    
    result = ''

    try:
        r = redis.StrictRedis(host=env.REDIS_HOST, port=env.REDIS_PORT, password='', decode_responses=True)
        
        if(r.exists('pubmed') == False):
            items = util.getPubmedData()
            resoure = json.dumps(items)
            preprocessed = util.englishPreprocessing(resoure)
            result = sorted(preprocessed, key=lambda x:x['value'], reverse=True)

            r.set("pubmed", json.dumps(result))
        else:
            result = json.loads(r.get('pubmed'))
   
    except Exception as e:
        print(e)
    
    res = jsonify(result)
    res.headers.add("Access-Control-Allow-Origin", "*")
    return res


@app.route('/crawl_pubmed')
def writeJson():
    json.dump(resource.getList(), open("templates/pubmed_data.json", "w"))

    res = jsonify({ 'result': 'ok' })
    res.headers.add("Access-Control-Allow-Origin", "*")
    return res

@app.route('/wordnet.html', methods=["GET"])
def view_wordgraph():
    return render_template("wordnetwork.html")

@app.route("/wordnet.json", methods=["GET"])
def getWordGraphData():
    
    
    env = config.getConfig()
    
    result = { 'nodes': [], 'links': [] }

    try:
        r = redis.StrictRedis(host=env.REDIS_HOST, port=env.REDIS_PORT, password='', decode_responses=True)
        
        if(r.exists('wordnetwork') == False):
            items = util.getPubmedData()
            
            nn_wordsSet = []

            node_df = generateNodes(items)

            for item in items:
                cleaned_content = util.eliminateUnnecessarySymbols(item)
                cleaned_content = util.caseConversion(cleaned_content)
                word_tokens = util.tokenizationWord(cleaned_content)

                # 품사 분리
                tokens_pos = util.taggingPOS(word_tokens)

                # 명사만 추출
                nn_words = util.extractOnlyNouns(tokens_pos)
                
                # 원형(lemma) 찾기
                lemmatized_words = util.lemmatization(nn_words)

                # 불용어 제거
                final_NN_words = util.stopwordsRemoval(lemmatized_words)

                nn_wordsSet.append(final_NN_words)

            
            network_df = generateLinks(nn_wordsSet)


            nodes = []
            links = []
            for index, row in node_df.iterrows():
                nodes.append({ 'node': row['node'], 'nodesize': row['nodesize'] })

            for index, row in network_df.iterrows():
                links.append({ 'source': row['source'], 'target': row['target'], 'support': row['support'] })

            resultSet = { 
                'nodes': nodes,
                'links': links
            }
            result = resultSet
            
            r.set("wordnetwork", json.dumps(resultSet))
        else:
            result = json.loads(r.get('wordnetwork'))
   
    except Exception as e:
        print(e)


    G = nx.Graph()

    nodesize = 0
    weight = 0

    if('nodesize' in request.args):
        nodesize = int(request.args.get('nodesize'))

    if('weight' in request.args):
        weight = float(request.args.get('weight'))

    for node in result['nodes']:
        if(node['nodesize'] > nodesize):
            G.add_node(node['node'], nodesize=node['nodesize'])

    for link in result['links']:
        if(link['support'] > weight):
            G.add_weighted_edges_from([(link['source'], link['target'], link['support'])])

    
    d = json_graph.node_link_data(G)

    res = jsonify(d)
    res.headers.add("Access-Control-Allow-Origin", "*")
    
    return res


def generateNodes(items):
    cleaned_content = util.eliminateUnnecessarySymbols("".join(items))
    cleaned_content = util.caseConversion(cleaned_content)
    word_tokens = util.tokenizationWord(cleaned_content)

    # 품사 분리
    tokens_pos = util.taggingPOS(word_tokens)

    # 명사만 추출
    nn_words = util.extractOnlyNouns(tokens_pos)
    
    # 원형(lemma) 찾기
    lemmatized_words = util.lemmatization(nn_words)

    # 불용어 제거
    final_NN_words = util.stopwordsRemoval(lemmatized_words)

    c = Counter(final_NN_words)

    node_df = pd.DataFrame(c.items(), columns=['node', 'nodesize'])
    node_df = node_df[node_df['nodesize'] >= 10].sort_values(by='nodesize', ascending=False)
    node_df.head()

    return node_df


def generateLinks(nn_wordsSet):
    results = list(apriori(nn_wordsSet,
        min_support=0.001))

    columns = ['source', 'target', 'support']
    network_df = pd.DataFrame(columns=columns)

    for result in results:
        if len(result.items) == 2:
            items = [x for x in result.items]
            row = [items[0], items[1], result.support]
            series = pd.Series(row, index=network_df.columns)
            network_df = network_df.append(series, ignore_index=True)
    
    network_df.head()
    
    return network_df


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)