import nltk
from nltk.tokenize.treebank import TreebankWordDetokenizer
import pickle
from nltk.corpus import stopwords
import re
from collections import Counter
import json

def getPubmedData():
    with open('templates/pubmed_data.json') as json_file:
        data = json.load(json_file)
        return data

def englishPreprocessing(data):
    # 불필요한 심볼젝거
    cleaned_content = eliminateUnnecessarySymbols(data)

    # 대문자를 소문자로 전환
    cleaned_content = caseConversion(cleaned_content)

    # 각각의 워드를 토큰으로 분리
    word_tokens = tokenizationWord(cleaned_content)

    # 품사 분리
    tokens_pos = taggingPOS(word_tokens)

    # 명사만 추출
    nn_words = extractOnlyNouns(tokens_pos)
    
    # 원형(lemma) 찾기
    lemmatized_words = lemmatization(nn_words)

    # 불용어 제거
    final_NN_words = stopwordsRemoval(lemmatized_words)

    # 빈도 분석
    analysisData = getFrequencyAnalysisResult(final_NN_words)

    return analysisData

def eliminateUnnecessarySymbols(data):
     # 문장단위로 끊기
    return re.sub(r'[^\.\?\!\w\d\s]','', data)

def caseConversion(cleaned_content):
    return cleaned_content.lower()

def tokenizationWord(cleaned_content):
    return nltk.word_tokenize(cleaned_content)

def deTokenizationWord(word_tokens):
    return TreebankWordDetokenizer().detokenize(word_tokens)

def taggingPOS(word_tokens):
    return nltk.pos_tag(word_tokens)

def extractOnlyNouns(tokens_pos):
    NN_words = []
    for word, pos in tokens_pos:
        if 'NN' in pos:
            NN_words.append(word)

    return NN_words

def lemmatization(nn_words):
    # nltk에서 제공되는 WordNetLemmatizer을 이용
    # ex) 명사의 경우는 보통 복수 -> 단수 형태로 변형
    wlem = nltk.WordNetLemmatizer()
    lemmatized_words = []
    for word in nn_words:
        new_word = wlem.lemmatize(word)
        lemmatized_words.append(new_word)

    return lemmatized_words

def stopwordsRemoval(lemmatized_words):
     #nltk에서 제공하는 불용어사전
    stopwords_list = stopwords.words('english')
    unique_NN_words = set(lemmatized_words)
    final_NN_words = lemmatized_words

    # 불용어 제거
    for word in unique_NN_words:
        if word in stopwords_list:
            while word in final_NN_words: final_NN_words.remove(word)

    return final_NN_words

def getFrequencyAnalysisResult(final_NN_words):
    datas = []

    c = Counter(final_NN_words)
    for k, v in c.items():
        datas.append({"key": k, "value": v })
        
    return datas